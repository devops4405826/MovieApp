# Kinopoisk web Service
## Description of the repository
This service is under development. The repository contains the source code of the Spring Boot application, nginx configuration and Dockerfile for running two containers (Spring server and Nginx proxy-web server). This application provides access to some kinopoiskAPI features through a browser.
## Technologies Used:

1. Java
2. Spring Boot
3. Docker
4. Maven
5. Nginx
6. HTML
7. redis
8. prometheus
9. loki
10. grafana
11. kubernetes
12. argocd
13. helm
## Example
###### *GET* request to get info about any film by its title:
```
http://192.168.1.111:8088/movie/info?title=Мстители:Финал
```
```json
{
  "docs":[{"id":843650,
    "name":"Мстители: Финал",
    "alternativeName":"Avengers: Endgame",
    "enName":"",
    "type":"movie",
    "year":2019,
    "description":"Оставшиеся в живых члены команды Мстителей и их союзники должны разработать новый план, который поможет противостоять разрушительным действиям могущественного титана Таноса. После наиболее масштабной и трагической битвы в истории они не могут допустить ошибку.",
    "shortDescription":"Железный человек, Тор и другие пытаются переиграть Таноса. Эпохальное завершение супергеройской франшизы",
    "movieLength":181,
    "isSeries":false,
    "ticketsOnSale":false,
    "totalSeriesLength":null,
    "seriesLength":null,
    "ratingMpaa":"pg13",
    "ageRating":18,
    "top10":null,
    "top250":null,
    "typeNumber":1,
    "status":null,
    "names":[{"name":"Мстители: Финал"}]
  }]
}
```

# Документация приложения

## Описание
Это приложение представляет собой микросервисную архитектуру приложения, позволяющее получить доступ к различным фильмам , включающую несколько компонентов для обработки данных, API и управления базой данных. Проект также включает мониторинг и визуализацию метрик.

## Архитектура
Проект состоит из следующих компонентов:
- Redis
- PostgreSQL
- dbms-service
- api-service
- db-controller
- Prometheus
- Grafana
- loki
- promtail

## Компоненты

### Redis
Redis используется как кэш.
- **Image**: `redis:latest`
- **Container Name**: `redis`
- **Environment Variables**:
    - `REDIS_PASSWORD=191688`

### PostgreSQL
PostgreSQL используется как основная база данных.
- **Image**: `postgres:latest`
- **Container Name**: `db-container`
- **Environment Variables**:
    - `POSTGRES_USER=dbmsuser`
    - `POSTGRES_PASSWORD=191688`
    - `POSTGRES_DB=movieservicebd`
- **Volumes**:
    - `/home/wupdp/data:/var/lib/postgresql/data`
- **Ports**:
    - `5432:5432`

### dbms-service
Сервис для управления базой данных.
- **Build Context**: `./dbms-service`
- **Image**: `wupdp/dbms-service:latest`
- **Container Name**: `dbms-service`
- **Environment Variables**:
    - `DATABASE_URL=db-container:5432`
- **Volumes**:
    - `/home/wupdp/logs/dbms:/app/logs`
- **Ports**:
    - `8000:8080`

### api-service
API-сервис.
- **Build Context**: `./api-service`
- **Image**: `wupdp/api-service:latest`
- **Container Name**: `api-service`
- **Volumes**:
    - `/home/wupdp/logs/api:/app/logs`
- **Ports**:
    - `8081:8081`

### db-controller
Сервис для контроля базы данных.
- **Build Context**: `./db-controller`
- **Image**: `wupdp/db-controller`
- **Container Name**: `db-controller`
- **Volumes**:
    - `/home/wupdp/logs/controller:/app/logs`
- **Ports**:
    - `8088:8088`

### Prometheus
Система мониторинга и алертинга.
- **Image**: `prom/prometheus`
- **Container Name**: `prometheus`
- **Volumes**:
    - `./prometheus/prometheus.yml:/etc/prometheus/prometheus.yml`

### Grafana
Платформа для визуализации метрик.
- **Image**: `grafana/grafana`
- **Ports**:
    - `3000:3000`
- **Environment Variables**:
    - `GF_SECURITY_ADMIN_USER=admin`
    - `GF_SECURITY_ADMIN_PASSWORD=admin`

## CI/CD Пайплайн

### Сборка и деплой

Процесс CI/CD автоматизирован с использованием GitLab CI/CD. Ниже приведен пример `.gitlab-ci.yml`, используемого для сборки и деплоя всех микросервисов.

```yaml
variables:
  IMAGE_TAG: "latest"
stages:
  - build
  - test
  - deploy

# Сборка
build_dbms-service:
  stage: build
  script:
    - docker login -u $USER -p $PASSWORD
    - docker build -t $REGISTRY_PATH/dbms-service:$IMAGE_TAG ./dbms-service
    - docker push $REGISTRY_PATH/dbms-service:$IMAGE_TAG
  only:
    - main
  tags:
    - build

build_api-service:
  stage: build
  script:
    - docker login -u $USER -p $PASSWORD
    - docker build -t $REGISTRY_PATH/api-service:$IMAGE_TAG ./api-service
    - docker push $REGISTRY_PATH/api-service:$IMAGE_TAG
  only:
    - main
  tags:
    - build

build_db-controller:
  stage: build
  script:
    - docker login -u $USER -p $PASSWORD
    - docker build -t $REGISTRY_PATH/db-controller:$IMAGE_TAG ./db-controller
    - docker push $REGISTRY_PATH/db-controller:$IMAGE_TAG
  only:
    - main
  tags:
    - build

# Тестирование
test_dbms-service:
  stage: test
  image: maven:latest
  services:
    - docker:dind
  script:
    - cd dbms-service
    - mvn test
  only:
    - main
  tags:
    - test

test_api-service:
  stage: test
  image: maven:latest
  services:
    - docker:dind
  script:
    - cd api-service
    - mvn test
  only:
    - main
  tags:
    - test

test_db-controller:
  stage: test
  image: maven:latest
  services:
    - docker:dind
  script:
    - cd db-controller
    - mvn test
  only:
    - main
  tags:
    - test

# Деплой
deploy:
  stage: deploy
  script:
    - echo "Deploying to Kubernetes"
  only:
    - main
  tags:
    - deploy
```

На данном этапе CI заканчивается пушем docker images в dockerhub registry

# Deploy


Deploy реализован с использованием ArgoCD
Helm Chart и application.yml находятся в следующем репозитории
https://gitlab.com/devops4405826/movieappcharts.git
